{ stdenv, fetchurl, buildPythonPackage }:

buildPythonPackage rec {
  name = "pycovis-${version}";
  version = "0.5.0";

  src = fetchurl {
    url = "https://bitbucket.org/uwaploe/py-covis/get/release-${version}.tar.gz";
    sha256 = "";
  };

  doCheck = false;

  meta = {
    description = "Python package for the COVIS-2 Project";
    license = stdenv.lib.licenses.gpl2;
    platform = stdenv.lib.platforms.linux;
  };
}