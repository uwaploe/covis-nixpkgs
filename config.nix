# Packages for the sysop account on COVIS
with import <nixpkgs> {};
{
  packageOverrides = pkgs: with pkgs; rec {
    pycovis = callPackage ./pkgs/pycovis {
      inherit fetchurl;
      buildPythonPackage = python36Packages.buildPythonPackage;
    };
    # This will supply most of the Python packages that we need. The
    # rest will need to be installed by pip in a virtualenv
    pyenv = python36.withPackages (ps: with ps; [
      pip
      virtualenv
      pyserial
      click
      cmd2
      protobuf3_3
      redis
      gevent
    ]);

    covis = buildEnv {
      name = "all-covis";
      paths = [
        pyenv
        stow
        inotify-tools
        jq
        go
        pkgconfig
      ];
    };
  };
}